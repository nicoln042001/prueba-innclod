# PRUEBA TECNICA PRACTICA PHP INGENIERO DE DESARROLLO

A continuación les presentaré los detalles de mi prueba técnica desarrollada en el framework Laravel. Permite a los usuarios gestionar documentos relacionados con procesos y tipos de documentos. A continuación, brindaré la información detallada sobre cómo instalar y utilizar esta aplicación.

## Requisitos

- Servidor web (por ejemplo, XAMPP).
- PHP 7.x o superior.
- Composer instalado.
- Base de datos MySQL o compatible.
 
## Instalación

1. Clona el repositorio desde GitLab:

   ```bash
   git clone https://gitlab.com/nicoln042001/prueba-innclod.git
   ```

2. Navega al directorio del proyecto:

   ```bash
   cd prueba-innclod
   ```

3. Copia el archivo de configuración de ejemplo y renómbralo:

   ```bash
   cp .env.example .env
   ```

4. Abre el archivo `.env` y configura la conexión a la base de datos con los detalles de tu servidor.

5. Ejecuta el comando `composer install` para instalar las dependencias de Laravel.

6. Ejecuta el comando `php artisan key:generate` para generar una clave de aplicación.

7. Ejecuta el comando `php artisan migrate --seed` para aplicar las migraciones y seeders y crear la estructura de la base de datos.

8. Inicia el servidor de desarrollo de Laravel:

   ```bash
   php artisan serve
   ```

9. Abre un navegador y accede a la aplicación en la URL proporcionada. Deberías ver la página de Bienvenida.

## Inicio de Sesión

Utiliza las siguientes credenciales para iniciar sesión en la aplicación:

- Usuario: `admin@prueba.com`
- Contraseña: `123456789`

## Uso

Una vez iniciada la sesión, podrás utilizar las siguientes funciones:

- **Listar Documentos**: Ver la lista de documentos.
- **Crear Documento**: Agregar un nuevo documento.
- **Editar Documento**: Modificar un documento existente.
- **Eliminar Documento**: Eliminar un documento.

## Generación de Códigos Únicos

La aplicación genera códigos únicos consecutivos para cada documento siguiendo el formato especificado (TIP_PREFIJO - PRO_PREFIJO - «Consecutivo único»).

## Vídeo de demostración
[![Ver Video]](https://youtu.be/3mfHJOVASE8 "Ver Video")

## ¡GRACIAS!
