-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-11-2023 a las 06:45:48
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba_innclod`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doc_documentos`
--

CREATE TABLE `doc_documentos` (
  `doc_id` bigint(20) UNSIGNED NOT NULL,
  `doc_nombre` varchar(55) NOT NULL,
  `doc_codigo` varchar(255) DEFAULT NULL,
  `doc_contenido` longtext NOT NULL,
  `doc_id_tipo` bigint(20) UNSIGNED NOT NULL,
  `doc_id_proceso` bigint(20) UNSIGNED NOT NULL,
  `doc_id_usuario` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `doc_documentos`
--

INSERT INTO `doc_documentos` (`doc_id`, `doc_nombre`, `doc_codigo`, `doc_contenido`, `doc_id_tipo`, `doc_id_proceso`, `doc_id_usuario`, `created_at`, `updated_at`) VALUES
(1, '50', 'REC-ING-1', 'dsfsfdsf', 5, 1, 1, '2023-11-08 10:01:41', '2023-11-08 10:01:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pro_procesos`
--

CREATE TABLE `pro_procesos` (
  `pro_id` bigint(20) UNSIGNED NOT NULL,
  `pro_prefijo` varchar(255) NOT NULL,
  `pro_nombre` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pro_procesos`
--

INSERT INTO `pro_procesos` (`pro_id`, `pro_prefijo`, `pro_nombre`, `created_at`, `updated_at`) VALUES
(1, 'ING', 'Ingeniería', '2023-11-08 10:01:36', '2023-11-08 10:01:36'),
(2, 'DIS', 'Diseño', '2023-11-08 10:01:36', '2023-11-08 10:01:36'),
(3, 'GES', 'Gestión', '2023-11-08 10:01:36', '2023-11-08 10:01:36'),
(4, 'DES', 'Desarrollo', '2023-11-08 10:01:36', '2023-11-08 10:01:36'),
(5, 'QA', 'Control de calidad', '2023-11-08 10:01:36', '2023-11-08 10:01:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tip_tipo_docs`
--

CREATE TABLE `tip_tipo_docs` (
  `tip_id` bigint(20) UNSIGNED NOT NULL,
  `tip_nombre` varchar(255) NOT NULL,
  `tip_prefijo` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tip_tipo_docs`
--

INSERT INTO `tip_tipo_docs` (`tip_id`, `tip_nombre`, `tip_prefijo`, `created_at`, `updated_at`) VALUES
(1, 'Instructivo', 'INS', '2023-11-08 10:01:36', '2023-11-08 10:01:36'),
(2, 'Manual', 'MAN', '2023-11-08 10:01:36', '2023-11-08 10:01:36'),
(3, 'Guía', 'GUIA', '2023-11-08 10:01:36', '2023-11-08 10:01:36'),
(4, 'Pauta', 'PAU', '2023-11-08 10:01:36', '2023-11-08 10:01:36'),
(5, 'Receta', 'REC', '2023-11-08 10:01:36', '2023-11-08 10:01:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@prueba.com', NULL, '$2y$10$e171G8XA4vU1LfZQdautSeN8mvV4/SNbHU5QRawgGVOn75mZ1mjV6', NULL, '2023-11-08 10:01:36', '2023-11-08 10:01:36');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `doc_documentos`
--
ALTER TABLE `doc_documentos`
  ADD PRIMARY KEY (`doc_id`),
  ADD UNIQUE KEY `doc_documentos_doc_codigo_unique` (`doc_codigo`),
  ADD KEY `doc_documentos_doc_id_tipo_foreign` (`doc_id_tipo`),
  ADD KEY `doc_documentos_doc_id_proceso_foreign` (`doc_id_proceso`),
  ADD KEY `doc_documentos_doc_id_usuario_foreign` (`doc_id_usuario`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`email`);

--
-- Indices de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indices de la tabla `pro_procesos`
--
ALTER TABLE `pro_procesos`
  ADD PRIMARY KEY (`pro_id`);

--
-- Indices de la tabla `tip_tipo_docs`
--
ALTER TABLE `tip_tipo_docs`
  ADD PRIMARY KEY (`tip_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `doc_documentos`
--
ALTER TABLE `doc_documentos`
  MODIFY `doc_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pro_procesos`
--
ALTER TABLE `pro_procesos`
  MODIFY `pro_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tip_tipo_docs`
--
ALTER TABLE `tip_tipo_docs`
  MODIFY `tip_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `doc_documentos`
--
ALTER TABLE `doc_documentos`
  ADD CONSTRAINT `doc_documentos_doc_id_proceso_foreign` FOREIGN KEY (`doc_id_proceso`) REFERENCES `pro_procesos` (`pro_id`),
  ADD CONSTRAINT `doc_documentos_doc_id_tipo_foreign` FOREIGN KEY (`doc_id_tipo`) REFERENCES `tip_tipo_docs` (`tip_id`),
  ADD CONSTRAINT `doc_documentos_doc_id_usuario_foreign` FOREIGN KEY (`doc_id_usuario`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
