<?php

namespace Database\Seeders;

use App\Models\ProProceso;
use Illuminate\Database\Seeder;

class ProProcesoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProProceso::updateOrCreate(['pro_prefijo' => 'ING'], ['pro_nombre' => 'Ingeniería']);
        ProProceso::updateOrCreate(['pro_prefijo' => 'DIS'], ['pro_nombre' => 'Diseño']);
        ProProceso::updateOrCreate(['pro_prefijo' => 'GES'], ['pro_nombre' => 'Gestión']);
        ProProceso::updateOrCreate(['pro_prefijo' => 'DES'], ['pro_nombre' => 'Desarrollo']);
        ProProceso::updateOrCreate(['pro_prefijo' => 'QA'], ['pro_nombre' => 'Control de calidad']);
    }
}
