<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       $this->call([
        ProProcesoSeeder::class,
        TipTipoDocSeeder::class,
        UserSeeder::class
       ]);
    }
}
