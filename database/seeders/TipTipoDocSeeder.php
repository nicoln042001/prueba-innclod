<?php

namespace Database\Seeders;

use App\Models\TipTipoDoc;
use Illuminate\Database\Seeder;

class TipTipoDocSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipTipoDoc::updateOrCreate(['tip_prefijo' => 'INS'], ['tip_nombre' => 'Instructivo']);
        TipTipoDoc::updateOrCreate(['tip_prefijo' => 'MAN'], ['tip_nombre' => 'Manual']);
        TipTipoDoc::updateOrCreate(['tip_prefijo' => 'GUIA'], ['tip_nombre' => 'Guía']);
        TipTipoDoc::updateOrCreate(['tip_prefijo' => 'PAU'], ['tip_nombre' => 'Pauta']);
        TipTipoDoc::updateOrCreate(['tip_prefijo' => 'REC'], ['tip_nombre' => 'Receta']);
    }
}
