<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDOCDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_documentos', function (Blueprint $table) {
            $table->id('doc_id');
            $table->string('doc_nombre', 55);
            $table->string('doc_codigo')->unique()->nullable();
            $table->longText('doc_contenido', 4000);
            $table->unsignedBigInteger('doc_id_tipo');
            $table->unsignedBigInteger('doc_id_proceso');
            $table->unsignedBigInteger('doc_id_usuario');
            $table->timestamps();

            //foraneas
            $table->foreign('doc_id_tipo')->references('tip_id')->on('tip_tipo_docs');
            $table->foreign('doc_id_proceso')->references('pro_id')->on('pro_procesos');
            $table->foreign('doc_id_usuario')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('d_o_c_documentos');
    }
}
