<?php

use App\Http\Controllers\DocDocumentoController;
use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    
    //Documents Routes
    Route::get('/documentos', [DocDocumentoController::class, 'index'])->name('documentos');
    Route::get('/documentos/create', [DocDocumentoController::class, 'create'])->name('documentos.create');
    Route::get('/documentos/edit/{id}', [DocDocumentoController::class, 'edit'])->name('documentos.edit');
    Route::post('/documentos/store', [DocDocumentoController::class, 'store'])->name('documentos.store');
    Route::put('/documentos/update/{docId}', [DocDocumentoController::class, 'update'])->name('documentos.update');
    Route::delete('/documentos/destroy/{docId}', [DocDocumentoController::class, 'destroy'])->name('documentos.destroy');

});

require __DIR__.'/auth.php';
