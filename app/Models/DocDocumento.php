<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocDocumento extends Model
{
    use HasFactory;
    protected $table = 'doc_documentos';
    protected $primaryKey = 'doc_id';
    protected $fillable = [
        'doc_nombre',
        'doc_codigo',
        'doc_contenido',
        'doc_id_tipo',
        'doc_id_proceso',
        'doc_id_usuario'
    ];

    //relaciones
    public function tipTipoDocumento(){
        return $this->belongsTo(TipTipoDoc::class);
    }

    public function ProProceso(){
        return $this->belongsTo(ProProceso::class);
    }

    public function User(){
        return $this->belongsTo(User::class);
    }
}
