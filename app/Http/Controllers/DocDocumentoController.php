<?php

namespace App\Http\Controllers;

use App\Models\DocDocumento;
use App\Models\ProProceso;
use App\Models\TipTipoDoc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class DocDocumentoController extends Controller
{
    /**
     * método para el listado de los documentos
     */
    public function index()
    {
        $documents = DocDocumento::join('tip_tipo_docs', 'doc_documentos.doc_id_tipo', 'tip_tipo_docs.tip_id')
            ->join('pro_procesos', 'doc_documentos.doc_id_proceso', 'pro_procesos.pro_id')
            ->select(
                'doc_documentos.*',
                'tip_tipo_docs.tip_nombre',
                'pro_procesos.pro_nombre'
            )->get();

        return Inertia::render('documents/list', ['documents' => $documents]);
    }

    /**
     * vista de creación de documento
     */
    public function create()
    {
        $procesos = ProProceso::all();
        $tipDocs = TipTipoDoc::all();
        return Inertia::render('documents/create', ['procesos' => $procesos, 'tipDocs' => $tipDocs]);
    }

    /**
     * vista edicion de documento
     */
    public function edit($id)
    {
        $procesos = ProProceso::all();
        $tipDocs = TipTipoDoc::all();
        $documento = DocDocumento::find($id);
        return Inertia::render('documents/edit', ['documento' => $documento, 'procesos' => $procesos, 'tipDocs' => $tipDocs]);
    }

    /**
     * crear documento
     */
    public function store(Request $request)
    {
        $tipDoc = TipTipoDoc::find($request->tip_doc);
        $proceso = ProProceso::find($request->proceso);
        $document = DocDocumento::create([
            'doc_nombre' => $request->nombre,
            'doc_codigo' => '',
            'doc_contenido' => $request->contenido,
            'doc_id_tipo' => $request->tip_doc,
            'doc_id_proceso' => $request->proceso,
            'doc_id_usuario' => Auth::id()
        ]);

        $document->doc_codigo = "{$tipDoc->tip_prefijo}-{$proceso->pro_prefijo}-$document->doc_id";
        $document->save();

        return redirect('documentos');
    }

    /**
     * editar documentos
     */
    public function update(Request $request, $docId)
    {
        $tipDoc = TipTipoDoc::find($request->tip_doc);
        $proceso = ProProceso::find($request->proceso);
        $document = DocDocumento::find($docId);
        $document->doc_nombre = $request->nombre;
        $document->doc_codigo = "{$tipDoc->tip_prefijo}-{$proceso->pro_prefijo}-$document->doc_id";;
        $document->doc_contenido = $request->contenido;
        $document->doc_id_tipo = $request->tip_doc;
        $document->doc_id_proceso = $request->proceso;
        $document->save();

        return redirect('documentos');
    }

    /**
     * eliminar documento
     */
    public function destroy($docId)
    {
        $document = DocDocumento::find($docId);
        $document->delete();

        return redirect('documentos');
    }
}
